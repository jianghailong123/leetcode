class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        int flag = 1;
        for(int i = digits.size() - 1; i >= 0; i--) {
            if(digits[i] + flag < 10){
                // 不需要进位
                flag = 0;
                break;
            }else{
                digits[i] += 1;
                flag = 1;
            }
        }
        // 看最后一个flag是否为1
        if(flag) {
            //vector全部后移
            digits.push_back(1);
            reverse(digits.begin(), digits.end());
        }
        return digits;
    }
};